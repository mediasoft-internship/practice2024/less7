package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	go func() {
		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			fmt.Println("Received:", scanner.Text())
		}
	}()

	inputScanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Type message to send, 'exit' to quit:")
	for inputScanner.Scan() {
		text := inputScanner.Text()
		if strings.TrimSpace(text) == "exit" {
			break
		}
		conn.Write([]byte(text + "\n"))
	}
}
