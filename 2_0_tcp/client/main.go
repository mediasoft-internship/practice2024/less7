package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	consoleReader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter text to send, type 'exit' to quit:")
	for {
		input, _ := consoleReader.ReadString('\n')
		if strings.TrimSpace(input) == "exit" {
			break
		}
		conn.Write([]byte(input))
	}
}
