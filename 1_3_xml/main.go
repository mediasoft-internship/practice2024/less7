package main

import (
	"encoding/xml"
	"fmt"
)

type Person struct {
	XMLName xml.Name `xml:"person"`
	Name    string   `xml:"name"`
	Age     int      `xml:"age"`
}

func main() {
	p := &Person{Name: "Dave", Age: 40}
	xmlData, err := xml.MarshalIndent(p, "", "  ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(xmlData))
}
