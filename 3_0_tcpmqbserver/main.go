package main

import (
	"bufio"
	"fmt"
	"net"
	"sync"
)

var (
	mutex   sync.Mutex
	clients = make(map[net.Conn]struct{})
)

func handleClient(conn net.Conn) {
	defer func() {
		mutex.Lock()
		delete(clients, conn)
		mutex.Unlock()
		conn.Close()
	}()

	mutex.Lock()
	clients[conn] = struct{}{}
	mutex.Unlock()

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		message := scanner.Text()
		broadcastMessage(message, conn)
	}
}

func broadcastMessage(message string, sender net.Conn) {
	mutex.Lock()
	defer mutex.Unlock()
	for client := range clients {
		if client != sender {
			client.Write([]byte(message + "\n"))
		}
	}
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer ln.Close()
	fmt.Println("TCP Message Broker Server started on :8080")

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go handleClient(conn)
	}
}
