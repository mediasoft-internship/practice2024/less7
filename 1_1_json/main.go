package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type Person struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main() {
	marshal()   // Тут пример с преобразованием в json из go
	unmarshal() // Тут пример с преобразованием из json в go
}

func marshal() {
	p := Person{"Alice", 30}
	jsonData, err := json.Marshal(p)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(jsonData))
}

func unmarshal() {
	jsonData := []byte(`{"name": "Bob", "age": 25}`)
	var p Person
	err := json.Unmarshal(jsonData, &p)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+v\n", p)
}
