package main

import (
	"fmt"
	"net"
)

func main() {
	serverAddr := "127.0.0.1:8080"
	conn, err := net.Dial("udp", serverAddr)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	fmt.Println("Sending message to server...")
	_, err = conn.Write([]byte("Hello UDP Server!"))
	if err != nil {
		fmt.Println(err)
	}
}
