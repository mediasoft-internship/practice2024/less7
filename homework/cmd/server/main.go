package main

import "gitlab.com/mediasoft-internship/practice2024/less7/homework/internal/server"

func main() {
	server.Start()
}
