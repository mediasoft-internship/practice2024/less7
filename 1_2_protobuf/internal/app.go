package internal

import (
	"fmt"
	"log"

	"gitlab.com/mediasoft-internship/practice2024/less7/1_2_protobuf/pkg/api"
	"google.golang.org/protobuf/proto"
)

func Start() {
	person := &api.Person{Name: "Charlie", Age: 35}
	data, err := proto.Marshal(person)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(data)

	personResult := new(api.Person)
	err = proto.Unmarshal(data, personResult)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(personResult)
}
